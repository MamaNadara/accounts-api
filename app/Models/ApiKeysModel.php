<?php


namespace App\Models;


use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class ApiKeysModel extends  Model
{
    protected $table = 'api_keys';

    protected $fillable = [
        'api_name', 'api_key'
    ];


    //Check service api key
    public static function checkApiKey($params)
    {
        $checkApiKey = DB::table('api_keys')->where('api_key', $params->header('api-key'))->get();
        if($checkApiKey->isEmpty()) {
            return false;
        } else {
            return true;
        }
    }


    //Check service api key for other services
    public static function checkApiKeyForServiceCommunication($params)
    {
        $checkServiceApiKey = DB::table('api_keys')->where('api_key', $params->header('external-api-key'))->get();

        if($checkServiceApiKey->isEmpty()){
            return false;
        } else {
            return true;
        }
    }
}